# Basic pod info

![](assets/images/dne-dci-dcloud.png)

| Hostname | Description | IP Address | Credentials |
| --- | --- | --- | --- |
| **wkst1** | Dev Workstation: Windows | 198.18.133.36 | dcloud\demouser/C1sco12345 |
| **ubuntu** | Dev Workstation: Linux | 198.18.134.28 | cisco/C1sco12345 |
| **nxosv-1** | Nexus 9000v - 7.0.3(I2).1 | 198.18.134.140 | admin/C1sco12345 |
| **nxosv-2** | Nexus 9000v - 7.0.3(I2).1 | 198.18.134.141 | admin/C1sco12345 |
| **apic1** | ACI Simulator - 2.1.1h | 198.18.133.200 | admin/C1sco12345 |
| **centos1** | CentOS 7 Server - Automation Target | 198.18.134.49 | root/C1sco12345 or demouser/C1sco12345 |
| **centos2** | CentOS 7 Server - Automation Target | 198.18.134.50 | root/C1sco12345 or demouser/C1sco12345 |
| ucsctl1 | UCS Central - 1.5(1b) | 198.18.133.90 | admin/C1sco12345 |
| ucsm1 | UCS Manager Emulator | 198.18.133.91 | admin/C1sco12345 |
| ucsd | UCS Director - 6.0.1.1 | 198.18.133.112 | admin/C1sco12345 |
| win2012r2 | Windows 2012 Standard Server - Automation Target | 198.18.133.20 | dcloud\administrator/C1sco12345 |
| vc1 | vCenter 6.0 | 198.18.133.30 | administrator@vsphere.local/C1sco12345! |
| vesx1 | vSphere Host | 198.18.133.31 | root/C1sco12345 |
| vesx2 | vSphere Host | 198.18.133.32 | root/C1sco12345 |
| cimc1 | UCS IMC Emulator | 198.18.134.88 | root/C1sco12345 or admin/C1sco12345 |
| na-edge1 | vNetApp | 198.18.133.115 | root/C1sco12345 |
| ad1 | Domain Controller | 198.18.133.1 | administrator/C1sco12345 |
| ios-xe-mgmt.cisco.com | CSR1000v - 16.08.01 | ios-xe-mgmt.cisco.com:8181 | root/D_Vay!_10& |


## Getting connected

We will loosely follow Steps 2 through 4 at: https://learninglabs.cisco.com/tracks/devnet-express-dci/devnet-express-dci-prep/dne-dci-prep-01/step/2

1. Run Anyconnect client and fill in url with ```dcloud-sjc-anyconnect.cisco.com``` 

1. Click connect, and log in with the credentials from your assigned pod, listed below:

| Session Id |  Session Name |  Usernames |  Password |  Start |  Stop |  Public IPs |
| --- | --- | --- | --- | --- | --- | --- |
| 88307 | 1 - Cisco DevNet Express Data Center v2 | v20user1; v20user2; ... v20user16 | 99e36b | 6/4/18 8:30 | 6/9/18 6:30 | [128.107.222.153](https://128.107.222.153:8443) |
| 88306 | 2 - Cisco DevNet Express Data Center v2 | v1109user1; v1109user2; ... v1109user16 | bf253b | 6/4/18 8:34 | 6/9/18 6:26 | [128.107.222.156](https://128.107.222.156:8443) |
| 88305 | 3 - Cisco DevNet Express Data Center v2 | v546user1; v546user2; ... v546user16 | d73c69 | 6/4/18 8:38 | 6/9/18 6:22 | [128.107.222.157](https://128.107.222.157:8443) |
| 88304 | 4 - Cisco DevNet Express Data Center v2 | v703user1; v703user2; ... v703user16 | 421c1f | 6/4/18 8:42 | 6/9/18 6:18 | [128.107.222.158](https://128.107.222.158:8443) |
| 88303 | 5 - Cisco DevNet Express Data Center v2 | v115user1; v115user2; ... v115user16 | be1b98 | 6/4/18 8:46 | 6/9/18 6:14 | [128.107.222.159](https://128.107.222.159:8443) |
| 88302 | 6 - Cisco DevNet Express Data Center v2 | v1340user1; v1340user2; ... v1340user16 | 0a6976 | 6/4/18 8:50 | 6/9/18 6:10 | [128.107.222.160](https://128.107.222.160:8443) |
| 88301 | 7 - Cisco DevNet Express Data Center v2 | v735user1; v735user2; ... v735user16 | a3b503 | 6/4/18 8:54 | 6/9/18 6:06 | [128.107.222.161](https://128.107.222.161:8443) |
| 88300 | 8 - Cisco DevNet Express Data Center v2 | v462user1; v462user2; ... v462user16 | 13c077 | 6/4/18 8:58 | 6/9/18 6:02 | [128.107.222.162](https://128.107.222.162:8443) |
| 88299 | 9 - Cisco DevNet Express Data Center v2 | v775user1; v775user2; ... v775user16 | a98aac | 6/4/18 9:02 | 6/9/18 5:58 | [128.107.222.164](https://128.107.222.164:8443) |
| 88298 | 10 - Cisco DevNet Express Data Center v2 | v809user1; v809user2; ... v809user16 | 17ac73 | 6/4/18 9:06 | 6/9/18 5:54 | [128.107.222.165](https://128.107.222.165:8443) |
| 88297 | 11 - Cisco DevNet Express Data Center v2 | v654user1; v654user2; ... v654user16 | ab4a48 | 6/4/18 9:10 | 6/9/18 5:50 | [128.107.222.166](https://128.107.222.166:8443) |
| 88296 | 12 - Cisco DevNet Express Data Center v2 | v530user1; v530user2; ... v530user16 | d1638b | 6/4/18 9:14 | 6/9/18 5:46 | [128.107.222.168](https://128.107.222.168:8443) |
| 88295 | 13 - Cisco DevNet Express Data Center v2 | v135user1; v135user2; ... v135user16 | 1b2c29 | 6/4/18 9:18 | 6/9/18 5:42 | [128.107.222.169](https://128.107.222.169:8443) |
| 88294 | 14 - Cisco DevNet Express Data Center v2 | v147user1; v147user2; ... v147user16 | 8ef529 | 6/4/18 9:22 | 6/9/18 5:38 | [128.107.222.170](https://128.107.222.170:8443) |
| 88293 | 15 - Cisco DevNet Express Data Center v2 | v1244user1; v1244user2; ... v1244user16 | 99d79a | 6/4/18 9:26 | 6/9/18 5:34 | [128.107.222.171](https://128.107.222.171:8443) |
| 88292 | 16 - Cisco DevNet Express Data Center v2 | v364user1; v364user2; ... v364user16 | 01ce6e | 6/4/18 9:30 | 6/9/18 5:30 | [128.107.222.172](https://128.107.222.172:8443) |
| 88291 | 17 - Cisco DevNet Express Data Center v2 | v1151user1; v1151user2; ... v1151user16 | 5ab559 | 6/4/18 9:34 | 6/9/18 5:26 | [128.107.222.173](https://128.107.222.173:8443) |
| 88290 | 18 - Cisco DevNet Express Data Center v2 | v987user1; v987user2; ... v987user16 | 0b40f4 | 6/4/18 9:38 | 6/9/18 5:22 | [128.107.222.174](https://128.107.222.174:8443) |
| 88289 | 19 - Cisco DevNet Express Data Center v2 | v1103user1; v1103user2; ... v1103user16 | d852fb | 6/4/18 9:42 | 6/9/18 5:18 | [128.107.222.175](https://128.107.222.175:8443) |
| 88288 | 20 - Cisco DevNet Express Data Center v2 | v68user1; v68user2; ... v68user16 | ef42f8 | 6/4/18 9:46 | 6/9/18 5:14 | [128.107.222.177](https://128.107.222.177:8443) |
| 88287 | 21 - Cisco DevNet Express Data Center v2 | v604user1; v604user2; ... v604user16 | 65a60c | 6/4/18 9:50 | 6/9/18 5:10 | [128.107.222.133](https://128.107.222.133:8443) |
| 88286 | 22 - Cisco DevNet Express Data Center v2 | v320user1; v320user2; ... v320user16 | 4ebba0 | 6/4/18 9:54 | 6/9/18 5:06 | [128.107.222.178](https://128.107.222.178:8443) |
| 88285 | 23 - Cisco DevNet Express Data Center v2 | v1020user1; v1020user2; ... v1020user16 | 435665 | 6/4/18 9:58 | 6/9/18 5:02 | [128.107.222.180](https://128.107.222.180:8443) |
| 88284 | 24 - Cisco DevNet Express Data Center v2 | v605user1; v605user2; ... v605user16 | 85f1b6 | 6/4/18 10:02 | 6/9/18 4:58 | [128.107.222.182](https://128.107.222.182:8443) |
| 88283 | 25 - Cisco DevNet Express Data Center v2 | v553user1; v553user2; ... v553user16 | 864252 | 6/4/18 10:06 | 6/9/18 4:54 | [128.107.222.88](https://128.107.222.88:8443) |
| 88282 | 26 - Cisco DevNet Express Data Center v2 | v1178user1; v1178user2; ... v1178user16 | 36bd8e | 6/4/18 10:10 | 6/9/18 4:50 | [128.107.222.91](https://128.107.222.91:8443) |
| 88281 | 27 - Cisco DevNet Express Data Center v2 | v1459user1; v1459user2; ... v1459user16 | 34c55c | 6/4/18 10:14 | 6/9/18 4:46 | [128.107.222.89](https://128.107.222.89:8443) |
| 88280 | 28 - Cisco DevNet Express Data Center v2 | v1031user1; v1031user2; ... v1031user16 | b97f36 | 6/4/18 10:18 | 6/9/18 4:42 | [128.107.222.92](https://128.107.222.92:8443) |
| 88279 | 29 - Cisco DevNet Express Data Center v2 | v1119user1; v1119user2; ... v1119user16 | ca7613 | 6/4/18 10:22 | 6/9/18 4:38 | [128.107.222.183](https://128.107.222.183:8443) |
| 88278 | 30 - Cisco DevNet Express Data Center v2 | v950user1; v950user2; ... v950user16 | 394ce3 | 6/4/18 10:26 | 6/9/18 4:34 | [128.107.222.184](https://128.107.222.184:8443) |

1. If you do not have a VNC client, RDP to 198.18.133.36 with your preferred client. Log in with user: ```dcloud\demouser``` and password ```C1sco12345```

    1. From Jumphost Download VNC from https://www.realvnc.com/download/file/viewer.files/VNC-Viewer-6.17.1113-Windows.exe
    1. Install VNC viewer on Jumphost

1. Within VNC client, connect to 198.18.134.28:1

1. From Ubuntu Desktop, run "Terminal for coding"
![](assets/images/term-coding.png)

1. Choose "2" for Python 2

1. Run ```pip install ansible --upgrade```

1. Run ``ansible --version`` and confirm you are at verion 2.5.x

